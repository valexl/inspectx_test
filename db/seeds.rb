# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

# make_model_pairs = [{make: 'Toyota',  model: 'Prius'},
#                     {make: 'Toyota',  model: 'Camry'},
#                     {make: 'Honda',   model: 'Accord'},
#                     {make: 'Ferrari', model: 'Testarossa'},
#                     {make: 'Honda',   model: 'Civic'},
#                     {make: 'Tesla',   model: 'Model S'},
#                     {make: 'GMC',     model: 'Suburban'},
#                     {make: 'Ford',    model: 'Focus'},
#                     {make: 'Hyundai', model: 'Sonata'}
#                   ]

# Admin.create!(name:                  'Example User',
#              email:                 'admin@inspectx.com',
#              password:              'foobar',
#              password_confirmation: 'foobar',
#              activated:             true,
#              activated_at:          Time.zone.now)

# 99.times do |n|
#   name  = Faker::Name.name
#   email = Faker::Internet.safe_email
#   password = 'password'
#   Member.create!(name:  name,
#                  email: email,
#                  password:              password,
#                  password_confirmation: password,
#                  activated: true,
#                  activated_at: Time.zone.now)
# end

# 99.times do |n|
#   name  = Faker::Name.name
#   email = Faker::Internet.safe_email
#   password = "password"
#   mechanic = Mechanic.create!(name:                   name,
#                               email:                  email,
#                               password:               password,
#                               password_confirmation:  password,
#                               activated:              true,
#                               activated_at:           Time.zone.now)

#   bio     = Faker::Lorem.paragraph(10)
#   school  = 'Universal Technical Institute'
#   work    = Faker::Company.name + ' Automotive'
#   languages = ['English', 'French']
#   MechanicProfile.create!(mechanic_id: mechanic.id,
#                           bio:         bio,
#                           school:      school,
#                           work:        work,
#                           languages:   languages,
#                           cost:        rand(200) + 50)

#   Address.create!(mechanic_id: mechanic.id,
#                   street:      '1061 Market Street',
#                   city:        'San Francisco',
#                   state:       'CA',
#                   zip:         '94103')

#   years = [2009, 2010, 2011, 2012, 2013, 2014, 2014]

#   make_model_pairs.sample(2).each do |mp|
#     mechanic.specialties.create!(vehicle_year:  years.sample,
#                                  vehicle_make:  mp[:make],
#                                  vehicle_model: mp[:model])
#   end

#   n = rand(5)
#   (1..n).each do |n|
#     sampled_vehicle = make_model_pairs.sample
#     member = Member.offset(rand(Member.count)).first
#     mechanic.appointments.create!(member_id: member.id,
#                                   notes: Faker::Lorem.paragraph(3),
#                                   location: Faker::Address.street_address,
#                                   vehicle_year: years.sample,
#                                   vehicle_make: sampled_vehicle[:make],
#                                   vehicle_model: sampled_vehicle[:model],
#                                   seller_email: Faker::Internet.email,
#                                   start: DateTime.now + (n + 1).days,
#                                   end_time: DateTime.now + (n + 1).days + 1.hours
#                                   )
#   end

#   (1..5).each do |r|
#     member = Member.offset(rand(Member.count)).first
#     description = Faker::Lorem.paragraph
#     Rating.create!(mechanic_id: mechanic.id,
#                    member_id:   member.id,
#                    rating:      [3,4,5].sample,
#                    description: description
#                    )
#   end
# end
