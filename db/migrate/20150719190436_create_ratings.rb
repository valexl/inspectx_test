class CreateRatings < ActiveRecord::Migration
  def change
    create_table :ratings do |t|
      t.integer :mechanic_id
      t.integer :member_id
      t.integer :rating
      t.text    :description

      t.timestamps null: false
    end
  end
end
