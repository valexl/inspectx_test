class CreateAppointments < ActiveRecord::Migration
  def change
    create_table :appointments do |t|
      t.integer  :mechanic_id
      t.integer  :member_id
      t.datetime :start
      t.datetime :end_time
      t.text     :notes
      t.string   :mechanic_state
      t.string   :seller_state
      t.string   :admin_state
      t.string   :seller_email
      t.string   :location
      t.integer  :vehicle_year
      t.string   :vehicle_make
      t.string   :vehicle_model

      t.timestamps null: false
    end
  end
end
