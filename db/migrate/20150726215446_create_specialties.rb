class CreateSpecialties < ActiveRecord::Migration
  def change
    create_table :specialties do |t|
      t.string :vehicle_year
      t.string :vehicle_make
      t.string :vehicle_model
      t.integer :mechanic_idw

      t.timestamps null: false
    end
  end
end
