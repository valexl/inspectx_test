class AddCostToMechanicProfiles < ActiveRecord::Migration
  def change
    add_column :mechanic_profiles, :cost, :integer
  end
end
