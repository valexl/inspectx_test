class CreateMechanicProfiles < ActiveRecord::Migration
  def change
    create_table :mechanic_profiles do |t|
      t.integer :mechanic_id
      t.string :bio
      t.string :school
      t.string :work
      t.text :languages, array: true, default: []

      t.timestamps null: false
    end
  end
end
