class RemoveLanguagesFromMechanicProfiles < ActiveRecord::Migration
  def change
    remove_column :mechanic_profiles, :languages
  end
end
