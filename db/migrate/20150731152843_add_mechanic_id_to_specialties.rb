class AddMechanicIdToSpecialties < ActiveRecord::Migration
  def change
    add_column :specialties, :mechanic_id, :integer
    remove_column :specialties, :mechanic_idw
  end
end
