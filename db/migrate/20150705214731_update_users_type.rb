class UpdateUsersType < ActiveRecord::Migration
  def change
    User.all.update_all(type: 'Member')
  end
end
