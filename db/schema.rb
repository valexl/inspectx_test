# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150830005049) do

  create_table "addresses", force: :cascade do |t|
    t.integer  "mechanic_id"
    t.string   "street"
    t.string   "city"
    t.string   "state"
    t.string   "zip"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "appointments", force: :cascade do |t|
    t.integer  "mechanic_id"
    t.integer  "member_id"
    t.datetime "start"
    t.datetime "end_time"
    t.text     "notes"
    t.string   "mechanic_state"
    t.string   "seller_state"
    t.string   "admin_state"
    t.string   "seller_email"
    t.string   "location"
    t.integer  "vehicle_year"
    t.string   "vehicle_make"
    t.string   "vehicle_model"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "mechanic_profiles", force: :cascade do |t|
    t.integer  "mechanic_id"
    t.string   "bio"
    t.string   "school"
    t.string   "work"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "cost"
  end

  create_table "ratings", force: :cascade do |t|
    t.integer  "mechanic_id"
    t.integer  "member_id"
    t.integer  "rating"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "specialties", force: :cascade do |t|
    t.string   "vehicle_year"
    t.string   "vehicle_make"
    t.string   "vehicle_model"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.integer  "mechanic_id"
  end

  create_table "users", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.string   "password_digest"
    t.string   "remember_digest"
    t.string   "type"
    t.string   "activation_digest"
    t.boolean  "activated",         default: false
    t.datetime "activated_at"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true

end
