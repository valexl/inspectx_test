require 'rails_helper'

RSpec.describe StaticPagesController, type: :controller do
  [:home, :help, :about, :contact].each do |page|
    describe "GET \##{page.to_s}" do
      before {get page}
      it 'returns http success' do
        expect(response).to have_http_status(:success)
      end

      it 'renders the correct template' do
        expect(response).to render_template(page)
      end
    end
  end
end
