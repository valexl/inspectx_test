require 'rails_helper'

RSpec.describe MembersController, type: :controller do
  describe 'GET #index' do
    context 'a user who is not signed in' do
      it 'is unsuccessful' do
        get :index
        expect(response).to redirect_to '/login'
      end
    end
  end

  describe 'GET #new' do
    it 'returns http success' do
      get :new
      expect(response).to have_http_status(:success)
    end
  end

  describe 'PUT #update' do
    before {@user = FactoryGirl.create(:member)}
    context 'a valid user' do
      before {sign_in_as @user}
      it 'updates the user' do
        put :update, id: @user.id, member: {name: 'quazimoto', email: 'quazimoto@qqq.com'}
        @user.reload
        expect(@user.name).to eq('quazimoto')
        expect(@user.email).to eq('quazimoto@qqq.com')
      end
    end

    context 'a user who is not signed in' do
      it 'is unsuccessful' do
        put :update, id: @user.id, member: {name: 'quazimoto', email: 'quazimoto@qqq.com'}
        @user.reload
        expect(@user.name).to_not eq('quazimoto')
        expect(@user.email).to_not eq('quazimoto@qqq.com')
        expect(response).to redirect_to '/login'
      end
    end

    context 'the incorrect user' do
      before do
        different_user = FactoryGirl.create(:member, name: 'The Dude', email: 'dude@abides.com')
        sign_in_as different_user
      end

      it 'is unsuccessful' do
        put :update, id: @user.id, member: {name: 'quazimoto', email: 'quazimoto@qqq.com'}
        @user.reload
        expect(@user.name).to_not eq('quazimoto')
        expect(@user.email).to_not eq('quazimoto@qqq.com')
        expect(response).to redirect_to '/'
      end
    end
  end
end
