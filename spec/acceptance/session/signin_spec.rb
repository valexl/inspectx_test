require 'rails_helper'

describe 'the login process', :type => :feature do
  context 'a user with invalid credentials' do
    before do
      @user = FactoryGirl.build(:member)
      log_in @user
    end

    scenario 'rerenders the new form with a flash message' do
      expect(page).to have_content 'Log in'
      expect(page).to have_content 'Invalid email/password combination'
    end

    scenario 'only persists the flash for the single page view' do
      expect(page).to have_content 'Invalid email/password combination'
      click_on 'Home'
      expect(page).to_not have_content 'Invalid email/password combination'
    end

    scenario 'the user is not activated' do
      member = FactoryGirl.create(:unregistered_member, name: 'cheester')
      visit member_path(member)
      expect(page).to_not have_content(member.name)
      expect(page).to have_content('Please log in')
      log_in member
      expect(page).to have_content('Account not activated. Check your email for the activation link')
    end
  end

  context 'a user with valid credentials' do
    before {@user = FactoryGirl.create(:member)}
    scenario 'changes the header layout on login' do
      visit login_path
      expect(page).to have_content 'Home'
      expect(page).to have_content 'Help'
      expect(page).to have_content 'About', count: 2
      log_in @user
      expect(page).to have_content 'Home'
      expect(page).to have_content 'Help'
      expect(page).to have_content 'About', count: 1
      expect(page).to have_content 'Account'
    end
  end

  context 'a user who has successfully logged in' do
    before {@user = FactoryGirl.create(:member)}
    scenario 'can log out' do
      log_in @user
      click_link 'Account'
      click_link 'Log out'
      expect(page).to have_content 'Home'
      expect(page).to have_content 'Help'
      expect(page).to have_content 'About', count: 2
    end
  end
end
