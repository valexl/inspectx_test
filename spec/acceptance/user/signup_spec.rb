require 'rails_helper'

describe 'the member signup process', type: :feature do
  it 'creates a new user and sends them an activation email' do
    before_count = Member.count
    sign_up_with('fakey', 'faker@faker.com', 'fakefake88')
    expect(page).to have_content 'Please check you email to activate your account'
    expect(Member.count).to eq(before_count + 1)
    new_member = Member.last
    expect(new_member.name).to eq('fakey')
    expect(new_member.email).to eq('faker@faker.com')
    expect(new_member.activated).to eq(false)
    expect(ActionMailer::Base.deliveries.count).to eq(1)
  end

  it 'the user activates their account through the activation link' do
    member = FactoryGirl.create(:unregistered_member, name: 'cheester')
    expect(member.activated).to eq(false)
    visit edit_account_activation_path(member.activation_token, email: member.email)
    expect(page).to have_content 'Account activated!'
    expect(page).to have_content(member.name)
  end
end
