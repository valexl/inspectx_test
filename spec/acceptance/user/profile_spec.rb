require 'rails_helper'

describe 'profile_management', type: :feature do
  before {@user = FactoryGirl.create(:member)}
  context 'authorized user' do
    it 'allows the user to edit their profile' do
      expect(@user.name).to_not eq('FakestNameEver')
      log_in @user
      click_on 'Account'
      click_on 'Settings'
      fill_in 'Name', with: 'FakestNameEver'
      fill_in 'Password', with: @user.password
      fill_in 'Confirmation', with: @user.password
      click_button 'Save changes'
      expect(page).to have_content 'FakestNameEver'
      expect(page).to have_content 'Successful update!'
      expect(@user.reload.name).to eq('FakestNameEver')
    end
  end

  context 'user who has not yet logged in' do
    it 'redirects them to the root page with a flash message' do
      visit root_path
      visit edit_member_path(@user)
      page.should have_content 'Log in'
      page.should have_content "Please log in"
    end

    it 'forwards them to the right page on successful log in' do
      visit root_path
      visit edit_member_path(@user)
      page.should have_content 'Log in'
      fill_in_signin_form @user
      page.should have_content 'Update your profile'
    end
  end

  context 'unauthorized user' do
    before do
      different_user = FactoryGirl.create(:member, name: 'The Dude', email: 'dude@abides.com')
      log_in different_user
    end

    it 'redirects them to the root page with a flash message' do
      visit root_path
      visit edit_member_path(@user)
      page.should have_content 'You are not authorized to access this page'
    end
  end
end
