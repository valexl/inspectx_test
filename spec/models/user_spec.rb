require 'rails_helper'

RSpec.describe Member, type: :model do
  before { @user = FactoryGirl.build(:member) }

  describe '#authenticated?' do
    it 'should return false for a user with a nil digest' do
      @user.save!
      expect(@user.authenticated?(:remember, '')).to eq(false)
    end
  end

  describe 'validations' do
    describe 'password' do
      it 'should be present(nonblank)' do
        @user.password = @user.password_confirmation = ' ' * 6
        expect(@user).to_not be_valid
      end

      it 'should be at least 6 characters long' do
        @user.password = @user.password_confirmation = 'a' * 5
        expect(@user).to_not be_valid
      end
    end

    describe 'name' do
      it 'should be present' do
        @user.name = nil
        expect(@user).to_not be_valid
      end

      it 'should not be too long' do
        @user.name = 'a' * 51
        expect(@user).to_not be_valid
      end
    end

    describe 'email' do
      it 'should be present' do
        @user.email = nil
        expect(@user).to_not be_valid
      end

      it 'should not be too long' do
        @user.email = 'a' * 266
        expect(@user).to_not be_valid
      end

      it 'should not have invalid format' do
        invalid_addresses = %w[user@example,com user_at_foo.org user.name@example.
                           foo@bar_baz.com foo@bar+baz.com]
        invalid_addresses.each do |invalid_address|
          @user.email = invalid_address
          expect(@user).to_not be_valid
        end
      end

      it 'should be unique' do
        @invalid_user = @user.dup
        @user.save!
        expect(@invalid_user).to_not be_valid
      end

      it 'should be unique regardless of case' do
        @user.save!
        @invalid_user = FactoryGirl.build(:member, email: @user.email.upcase)
        expect(@invalid_user).to_not be_valid
      end
    end
  end
end
