FactoryGirl.define do
  factory :member do
    name                  'Fakey'
    email                 'fakey@example.com'
    password              'password'
    password_confirmation 'password'
    activated             true
    activated_at          {Time.zone.now}
    type                  'Member'

    factory :unregistered_member do
      activated            false
      activated_at         nil
    end
  end
end
