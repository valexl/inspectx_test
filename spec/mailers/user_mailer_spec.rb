require "rails_helper"

RSpec.describe UserMailer, type: :mailer do
  before {@member = FactoryGirl.create(:member)}
  describe "member_account_activation" do
    let(:mail) { UserMailer.member_account_activation(@member) }

    it "renders the headers" do
      expect(mail.subject).to eq("Account Activation")
      expect(mail.to).to eq([@member.email])
      expect(mail.from).to eq(["noreply@inspect-x.com"])
    end

    it "renders the body" do
      expect(mail.body.encoded).to match("Hi")
    end
  end

  describe "password_reset" do
    let(:mail) { UserMailer.password_reset(@member) }

    it "renders the headers" do
      expect(mail.subject).to eq("Password reset")
      expect(mail.to).to eq([@member.email])
      expect(mail.from).to eq(["noreply@inspect-x.com"])
    end

    it "renders the body" do
      expect(mail.body.encoded).to match("Hi")
    end
  end

end
