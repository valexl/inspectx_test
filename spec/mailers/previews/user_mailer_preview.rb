# Preview all emails at http://localhost:3000/rails/mailers/user_mailer
class UserMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/user_mailer/member_account_activation
  def member_account_activation
    @member = Member.last
    @member.activation_token = User.new_token
    @member.activation_digest = User.digest(@member.activation_token)
    @member.save!
    UserMailer.member_account_activation(@member)
  end

  def notify_seller
    @appointment = Appointment.pending.last
    UserMailer.notify_seller(@appointment)
  end

  # Preview this email at http://localhost:3000/rails/mailers/user_mailer/password_reset
  def password_reset
    UserMailer.password_reset
  end

end
