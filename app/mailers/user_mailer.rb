class UserMailer < ApplicationMailer
  def member_account_activation(member)
    @member = member
    mail to: @member.email, subject: 'Account Activation'
  end
  
  def mechanic_account_activation(mechanic)
    @mechanic = mechanic
    mail to: @mechanic.email, subject: 'Account Activation'
  end

  def approve_new_mechanic(mechanic)
    @mechanic = mechanic
    Admin.all.each do |admin|
      mail to: admin.email, subject: "New Mechanic #{@mechanic.name}"
    end
  end

  def mech_profile_update(mechanic)
    @mechanic = mechanic
    Admin.all.each do |admin|
      mail to: admin.email, subject: "Mechanic Profile Update (#{@mechanic.name})"
    end
  end

  def notify_seller(appointment)
    @appointment = appointment
    @seller_email = appointment.seller_email
    @user = appointment.member
    mail to: @seller_email, subject: "#{@user.name} requested an inspection"
  end

  def password_reset(user)
    @user = user
    mail to: @user.email
  end
end
