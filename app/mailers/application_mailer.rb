class ApplicationMailer < ActionMailer::Base
  default from: "noreply@inspect-x.com"
  layout 'mailer'
end
