class MechanicsController < ApplicationController
  before_action :logged_in_user, only: [:index,:show, :edit, :update]
  before_action :correct_user,   only: [:edit, :update]
  before_action :grab_params, only: [:index]

  def index
    @mechanics = search(Mechanic).paginate(page: params[:page])
  end

  def new
    @user = Mechanic.new
  end

  def create
    @user = Mechanic.new(mechanic_params)
    if @user.save
      @user.send_activation_email
      flash[:info] = 'Please check your email to activate your account'
      redirect_to root_url
    else
      render 'new'
    end
  end

  def show
    @user = Mechanic.find(params[:id])
    @appointments = @user.appointments
  end

  def edit
    @user.send_profile_update_email
    flash[:info] = 'Update request sent to admins'
    redirect_to mechanic_url(id: params[:id])
  end

protected

  def grab_params
    session[:make] = params[:make].try(:capitalize)
    session[:model] = params[:model]
    session[:year] = params[:year]
  end

  def search(mechanics)
    mechanics = mechanics.includes(:address, :mechanic_profile, :specialties).references(:specialties).where(addresses: {zip: params[:zip]})

    if mechanics.count > 20 && params[:make]
      mech = mechanics.where(specialties: {vehicle_make: params[:make].capitalize})
      if mech.count >= 5
        mechanics = mech
      end
    end

    # if mechanics.count > 20 && params[:model]
    #   mech = mechanics.where(specialties: {vehicle_model: params[:model].capitalize})
    #   if mech.count >= 5
    #     mechanics = mech
    #   end
    # end

    mechanics
  end

  def mechanic_params
    params.require(:mechanic).permit(:name, :email, :password, :password_confirmation,
                                      mechanic_profile_attributes: [:school, :work, :bio, :cost],
                                      address_attributes: [:street, :city, :state, :zip],
                                      specialties_attributes: [
                                        specialty_one: [:vehicle_make],
                                        specialty_two: [:vehicle_make]
                                        # specialty_one: [:vehicle_year, :vehicle_make, :vehicle_model],
                                        # specialty_two: [:vehicle_year, :vehicle_make, :vehicle_model]
                                        ])
  end
end
