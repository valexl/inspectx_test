class AccountActivationsController < ApplicationController
  def edit
    user = User.find_by(email: params[:email])
    if user && !user.activated? && user.authenticated?(:activation, params[:id])
      user.activate
      log_in user
      flash[:success] = "Account activated!"
      redirect_to user
    else
      flash[:danger] = "Invalid activation link"
      redirect_to root_url
    end
  end

  def approve
    user = User.find_by(email: params[:email])
    if user && user.activated?
      flash[:info] = "Mechanic already approved."
      redirect_to user
    elsif user
      user.activate
      flash[:success] = "Mechanic approved!"
      redirect_to user
    else
      flash[:danger] = "Invalid activation link"
      redirect_to root_url
    end
  end
end
