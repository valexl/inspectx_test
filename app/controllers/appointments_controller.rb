class AppointmentsController < ApplicationController
  before_action :logged_in_user, except: [:seller_approve, :seller_deny]
  before_action :find_mechanic

  def new
    @appointment = @mechanic.appointments.new(vehicle_year: session[:year], vehicle_make: session[:make], vehicle_model: session[:model])
  end

  def create
    @appointment = @mechanic.appointments.new(appointment_params)
    @appointment.member_id = current_user.id
    if @appointment.save
      flash[:success] = "Congratulations! Your appointment with #{@mechanic.name} has been booked"
      flash[:warning] = "We will be in touch to handle payment and finalize your appointment"
      redirect_to current_user
    else
      render 'new'
    end
  end

  def approve
    appointment = @mechanic.appointments.find(params[:appointment_id])
    if current_user.is_a? Mechanic
      appointment.update_attributes({mechanic_state: 'approved'})
      flash[:success] = 'Appointment Approved! If the appointment is not fully admin or seller approved, the appointment will be moved into current appointments after this process is complete.'
    elsif current_user.is_a? Admin
      appointment.update_attributes({admin_state: 'approved'})
      flash[:success] = 'Appointment Approved!'
      UserMailer.notify_seller(appointment).deliver_now
    end

    redirect_to current_user
  end

  def deny
    appointment = @mechanic.appointments.find(params[:appointment_id])
    if current_user.is_a? Mechanic
      appointment.update_attributes({mechanic_state: 'denied'})
      flash[:danger] = 'Appointment Denied. You can find the appointment in your archived appointments list'
    elsif current_user.is_a? Admin
      appointment.update_attributes({admin_state: 'denied'})
      flash[:danger] = 'Appointment Denied!'
    end

    redirect_to current_user
  end

  def seller_approve
    appointment = @mechanic.appointments.find(params[:appointment_id])
    appointment.update_attributes({seller_state: 'approved'})
    flash[:success] = 'Appointment Approved!. Make sure to keep track of the time and date so you dont miss your appointment'
    redirect_to root_path
  end

  def seller_deny
    appointment = @mechanic.appointments.find(params[:appointment_id])
    appointment.update_attributes({seller_state: 'denied'})
    flash[:danger] = 'Appointment Denied.'
    redirect_to root_path
  end

  def deparse_start
    DateTime.strptime(@appointment.start, '%m/%d/%Y %I:%M %p') if @appointment.start
  end

protected

  def appointment_params
    params.require(:appointment).permit(:start, :end, :notes, :seller_email, :location, :vehicle_year, :vehicle_make, :vehicle_model)
  end

  def find_mechanic
    @mechanic = Mechanic.find(params[:mechanic_id])
  end
end
