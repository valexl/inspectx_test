class StaticPagesController < ApplicationController
  def home
    @form = EdmundsApi.new make: params[:make], model: params[:model], year: params[:year]
  end

  def help
  end

  def about
  end

  def how_it_works
    @account_items = [
                      "Enter your ZIP code along with the year, make, and model of the vehicle you’d like inspected.",
                      "Choose 'Find a Mechanic' and view your search results––we leave mechanic selection entirely up to you. Select your mechanic based on price, reviews, availability, and certifications.",
                      "Click 'Book an Appointment' on your mechanic’s profile page. Enter the vehicle seller’s contact information and the desired inspection time and location. Your inspection request will be directed both to the seller and to your preferred mechanic for approval.",
                      "Complete the payment page. We’ll put a hold on your payment until the inspection has been completed and the mechanic has generated a satisfactory report. You’ll only be charged for a complete, thorough job.",
                      "After mechanic and seller approval, inspectX quality assurance will approve your request to begin the process. The inspection will take place at the specified time and place, and the mechanic will forward the report to inspectX for approval. We will complete the hold on your payment and send you the reviewed, approved inspection report.",
                      "Armed with a thorough inspection report, you’ll be ready to make an informed decision about your used vehicle purchase."
                      ]
  end
end
