class RatingsController < ApplicationController
  before_action :find_mechanic

  def create
    @rating =
    @mechanic.ratings.new(rating_params)
  end

protected
  def rating_params
    params.require(:rating).permit(:value, :description)
  end

  def find_mechanic
    @mechanic = Mechanic.find(params[:mechanic_id])
  end
end
