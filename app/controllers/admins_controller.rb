class AdminsController < ApplicationController
  before_action :logged_in_user
  before_action :correct_user

  def show
    @user = Admin.find(params[:id])
    @appointments = Appointment.all
  end
end
