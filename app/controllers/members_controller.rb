class MembersController < ApplicationController
  before_action :logged_in_user, only: [:index,:show, :edit, :update]
  before_action :correct_user,   only: [:edit, :show, :update]

  def index
  end

  def new
    @user = Member.new
  end

  def show
    @user = Member.find(params[:id])
    @appointments = @user.appointments
  end

  def create
    @user = Member.new(member_params)
    if @user.save
      @user.send_activation_email
      flash[:info] = 'Please check your email to activate your account'
      redirect_to root_url
    else
      render 'new'
    end
  end

  def edit
    @user = Member.find(params[:id])
  end

  def update
    @user = Member.find(params[:id])
    if @user.update_attributes(member_params)
      flash[:success] = 'Successful update!'
      redirect_to @user
    else
      render 'edit'
    end
  end

protected

  def member_params
    params.require(:member).permit(:name, :email, :password, :password_confirmation)
  end
end
