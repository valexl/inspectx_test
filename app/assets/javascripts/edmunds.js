var Edmunds = (function() {
    var urlBase = "https://api.edmunds.com/api/vehicle/v2";
    var makesByYear = "/makes?year=";
    var key = "pmyavfsh43b7zvt5wf8wpjum";
    var urlFooter = "&view=basic&fmt=json&api_key=";

    var makeGetRequest = function(url, onSuccess, onFailure) {
        $.ajax({
            type: 'GET',
            url: url,
            dataType: 'json',
            success: onSuccess,
            error: onFailure
        });
    };

    var attachMechSearchHandler = function() {

    };

    var attachCarMakeHandler = function() {
        $(".year-select").on("change", function() {
            var onSuccess = function(data) {
                populateMakes(data["makes"]);
            };
            var onFailure = function() {
                console.log("get request failure");
                window.alert("Error retrieving car makes");
            };
            var year = $(".year-select").val();
            console.log("year value = " + year);
            makeGetRequest(urlBase + makesByYear + year + urlFooter + key, onSuccess, onFailure);
        });
    };

    var populateMakes = function(makesList) {
        $(".make-select").html("");
        for (var i = 0; i < makesList.length; i++) {
            var makeObj = makesList[i];
            var makeName = makeObj["name"];
            $(".make-select").append("<option value='" + makeName + "'>"+ makeName);
        }
    };

    var start = function() {
        // init all vars and handlers here
        attachCarMakeHandler();
    };

    return {
        start: start
    };
})();

$(document).ready(function() {
    Edmunds.start();
    console.log("started6 edmunds.js");
});