class EdmundsApi
  include ActiveModel::Model
  include ActiveModel::Validations

  attr_accessor :make, :model, :year

  validates :make, presence: { message: 'make is required field' }

  def response
    return {} unless valid? #make is required field 
    begin
      if model.present? && year.present?
        get_info_by_year
      elsif model.present?
        get_info_by_model
      else
        get_info_by_make
      end
    rescue Edmunds::Api::Exception => e
      Rails.logger.error e.message
      {}      
    end
  end

  def get_make
    @get_make ||= Edmunds::Vehicle::Specification::Make::Make.find(make.to_s.downcase)  # ('audi')
  end

  def get_model
    @get_model ||= Edmunds::Vehicle::Specification::Model::Model.find(make.to_s.downcase, model.to_s.downcase) #('audi', 'tt')
  end

  def get_info_by_make
    info = get_make
    info.models.inject([]) do |res, itm| 
      postfix = "#{info.name} #{itm.name}"
      res += itm.years.map {|model_year| "#{model_year.year} #{postfix}"}
    end
  end

  def get_info_by_model
    postfix = "#{get_make.name} #{get_model.name}"
    get_model.years.map do |model_year|
      "#{model_year.year} #{postfix}"
    end
  end

  def get_info_by_year
    info = Edmunds::Vehicle::Specification::ModelYear::ModelYear.find(make.to_s.downcase, model.to_s.downcase, year) #('audi', 'tt', '2014')
    ["#{info.year} #{get_make.name} #{get_model.name}"]
  end

end
