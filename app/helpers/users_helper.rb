module UsersHelper
  def gravatar_for(user, img_class='gravatar')
    gravatar_id = Digest::MD5::hexdigest(user.email.downcase)
    gravatar_url = "https://secure.gravatar.com/avatar/#{gravatar_id}"
    # gravatar_url = 'http://robohash.org/' + "#{user.id}?set=set2"
    image_tag(gravatar_url, alt: user.name, class: img_class)
  end
end
