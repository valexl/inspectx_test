class Member < User
  has_many :ratings
  has_many :appointments

  def send_activation_email
    UserMailer.member_account_activation(self).deliver_now
  end
end
