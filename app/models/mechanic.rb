class Mechanic < User
  has_one :mechanic_profile
  has_one :address
  has_many :ratings
  has_many :appointments
  has_many :specialties

  accepts_nested_attributes_for :mechanic_profile, :address, :specialties

  delegate :bio, :school, :work, :languages, to: :mechanic_profile

  def send_activation_email
    UserMailer.mechanic_account_activation(self).deliver_now
  end

  def send_profile_update_email
    UserMailer.mech_profile_update(self).deliver_now
  end
end
