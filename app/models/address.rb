class Address < ActiveRecord::Base
  belongs_to :mechanic
  validates :street, :city, :state, :zip, presence: true
end
