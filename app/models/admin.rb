class Admin < User
  def send_activation_email
    UserMailer.member_account_activation(self).deliver_now
  end
end
