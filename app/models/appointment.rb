class Appointment < ActiveRecord::Base
  belongs_to :member
  belongs_to :mechanic

  validates :start, :location, :seller_email, :notes, :vehicle_year, :vehicle_make, :vehicle_model, presence: true
  validate :reasonable_start

  scope :pending, -> {where('mechanic_state != ? AND
                             admin_state != ? AND
                             seller_state != ?', 'denied', 'denied', 'denied')
                      .where('mechanic_state IS NULL OR
                              admin_state IS NULL OR
                              seller_state IS NULL')
                      .where('start > ?', DateTime.now)}
  scope :current, -> {where(mechanic_state: 'approved', admin_state: 'approved', seller_state: 'approved')
                      .where('start > ?', DateTime.now)}
  scope :archived, -> {where('mechanic_state = ? OR
                              admin_state = ? OR
                              seller_state = ? OR
                              start < ?', 'denied', 'denied', 'denied', DateTime.now)}

  before_save :find_end

  def pending_status(mechanic = 'mechanic')
    statuses = []
    statuses << 'Awaiting admin approval' if admin_state.nil?
    statuses << 'Awaiting seller approval' if seller_state.nil?
    statuses << "Awaiting #{mechanic} approval" if mechanic_state.nil?
    statuses.join(", ")
  end

  def find_end
    appointment_end = (start + 1.hours) if start
  end

  def reasonable_start
    if new_record? && start && (start < DateTime.now + 1.days)
      self.errors[:start] << 'time must be at least one day in advance'
    end
  end
end
