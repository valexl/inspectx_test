class MechanicProfile < ActiveRecord::Base
  belongs_to :mechanic

  validates :bio, :school, :work, :cost, presence: true

  serialize :languages
end
