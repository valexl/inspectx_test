Rails.application.routes.draw do
  root 'static_pages#home'
  get '/about',         to: 'static_pages#about',         as: 'about'
  get '/how_it_works',  to: 'static_pages#how_it_works',  as: 'how_it_works'
  get 'signup',   to: 'members#new',            as: 'signup'
  get    'login'   => 'sessions#new'
  post   'login'   => 'sessions#create'
  delete 'logout'  => 'sessions#destroy'
  resources :members
  resources :account_activations, only: [:edit]
  resources :mechanics do
    resources :appointments, only: [:new, :create] do
      get :approve
      get :deny
      get :seller_approve
      get :seller_deny
    end
  end
  resources :admins, only: [:show]
end
